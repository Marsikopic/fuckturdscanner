import { 
  createAppContainer,
  createStackNavigator,
  StackActions,
  NavigationActions
} from 'react-navigation';
import HomeScreen from "./HomeScreen"
import CameraScreen from "./CameraScreen"


const AppNavigator = createStackNavigator({
  Home: {
    screen: HomeScreen,
    header: { visible: true },
    navigationOptions: {
      title: 'Home',
    },
  },
  Camera: {
    screen: CameraScreen,
    header: { visible: true },
    navigationOptions: {
      title: 'Scan',
    },
 },
}, {
    initialRouteName: 'Home',
});

export default createAppContainer(AppNavigator);