import React from 'react';
import { 
  Camera,
  Permissions,
  FileSystem,
  MediaLibrary,
  ImageManipulator
} from 'expo';
import {
  View,
  TouchableOpacity,
  Platform,
  CameraRoll,
  Text,
  Button
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { 
  StackActions,
  NavigationActions
} from 'react-navigation';
import styles from './Styles';


class CameraScreen extends React.Component {
  state = {
    permissionsGranted: false,
    shouldPersistPhoto: false, // adjustable
  };

  componentDidMount() {
    this.askForPermissions();
  }

  askForPermissions = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL).catch(console.error);
    this.setState({ permissionsGranted: status === 'granted' });
  }

  takePhoto = async () => {
    let photo = await this.camera.takePictureAsync({
    exif: true
    });
    photo = await ImageManipulator.manipulateAsync(photo.uri, [{
        rotate: -photo.exif.Orientation
    }, {
        resize: {
            width: photo.width,
            height: photo.height
        }
    }], {
        compress: 1
    });
    let saveResult = await CameraRoll.saveToCameraRoll(photo.uri, 'photo');
    this.setState({ cameraRollUri: saveResult });
  };

  takePicture = async () => {
    if (this.camera) {
      const photo = await this.camera.takePictureAsync({
        exif: true, // adjust if needed
      }).catch(console.error);

      alert(JSON.stringify(photo));
      
      if (this.state.shouldPersistPhoto) {
        const asset = await MediaLibrary.createAssetAsync(photo.uri).catch(console.error);
        console.log('Saved asset', asset);
      }
    }
  };

  renderNoPermissions = () => 
    <View style={styles.noPermissions}>
      <Text style={styles.noPermissionsText}>
        Camera permissions not granted - cannot open camera preview.
      </Text>
    </View>

  renderBar = () =>
    <View style={styles.bar}>
      <TouchableOpacity
        onPress={this.takePhoto}
        style={styles.barButton}
      >
        <Ionicons name="ios-radio-button-on" size={70} color="white" />
      </TouchableOpacity>
    </View>

  renderCamera = () => (
    <View style={styles.cameraWrapper}>
      <Button title='Home' 
      onPress={() => {
            this.props.navigation.dispatch(StackActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({ routeName: 'Home' })
              ],
            }))
          }}
      />
      <Camera
        ref={ref => (this.camera = ref)}
        style={styles.camera}
        onMountError={console.error}
      />
      {this.renderBar()}
    </View>
  );

  render() {
    return <View style={styles.container}>
      { this.state.permissionsGranted
        ? this.renderCamera()
        : this.renderNoPermissions()
      }
    </View>;
  }
}

export default CameraScreen
