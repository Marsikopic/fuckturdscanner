import React from 'react';
import {
  View,
  Text,
  Button,
  CameraRoll,
  ActivityIndicator,
  ScrollView,
  Image,
  Alert,
} from 'react-native';
import { Permissions } from 'expo';
import { StackActions, NavigationActions } from 'react-navigation';
import styles from './Styles';

class PhotosDisplayer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      permissionsGranted: false,
      isPicturesDisplayed: false,
      photos: [],
    };
  }

  componentDidMount() {
    this.askForPermissions();
    this.ExtractPhotos();
  }

  askForPermissions = async () => {
    const { status } = await Permissions.askAsync(
      Permissions.CAMERA,
      Permissions.CAMERA_ROLL
    ).catch(console.error);
    this.setState({ permissionsGranted: status === 'granted' });
  };

  getPhotosArray = () => {
    return this.state.photos.map((p, i) => {
            return (
              <Image
                key={i}
                style={{
                  width: 400,
                  height: 500,
                }}
                source={{ uri: p.node.image.uri }}
              />
            );
          });
  }

  render() {
    if (!this.state.permissionsGranted) {
      return (
        <View style={styles.noPermissions}>
          <Text style={styles.noPermissionsText}>
            Camera permissions not granted - cannot open camera preview.
          </Text>
        </View>
      );
    }
    if (!this.state.isPicturesDisplayed) {
      return (
        <View
          style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <ActivityIndicator />
          <Text>Loading youre god damn pictures</Text>
        </View>
      );
    }

    return (
      <View>
        <ScrollView contentContainerStyle={{ alignItems: 'center' }}>
          {
              this.getPhotosArray()
          }
        </ScrollView>
      </View>
    );
  }

  ExtractPhotos = () => {
    CameraRoll.getPhotos({
      first: 100,
      assetType: 'Photos',
    })
      .then(r => {
        this.setState({ photos: r.edges, isPicturesDisplayed: true });
      })
      .catch(err => {
        //Error Loading Images
      });
  };
}

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isPicturesDisplayed: false, photos: null };
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <PhotosDisplayer />
        <View
          style={{
            flex: 1,
            alignItems: 'flex-end',
            justifyContent: 'flex-end',
          }}>
          <Button
            title="Camera"
            style={{ alignItems: 'center' }}
            onPress={() => {
              this.props.navigation.dispatch(
                StackActions.reset({
                  index: 0,
                  actions: [
                    NavigationActions.navigate({ routeName: 'Camera' }),
                  ],
                })
              );
            }}>
            <Text>Camera</Text>
          </Button>
        </View>
      </View>
    );
  }
}

export default HomeScreen;
