import {
    StyleSheet,
  } from 'react-native';
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'black',
    },
    cameraWrapper: {
      flex: 1,
    },
    camera: {
      flex: 1,
    },
    bar: {
      paddingBottom: 5,
      backgroundColor: 'transparent',
      alignSelf: 'center',
      flex: 0.2,
      flexDirection: 'row',
    },
    barButton: {
      alignSelf: 'center',
    },
    noPermissions: {
      flex: 1,
      alignItems:'center',
      justifyContent: 'center',
      padding: 10,
    },
    noPermissionsText: {
      color: 'white',
    }
  });
  